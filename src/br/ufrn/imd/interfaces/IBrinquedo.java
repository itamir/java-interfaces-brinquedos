package br.ufrn.imd.interfaces;

public interface IBrinquedo {

	public void mover();
	
	public void setNome(String nome);
	
}
