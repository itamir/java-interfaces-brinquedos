package br.ufrn.imd.dominio;

import br.ufrn.imd.interfaces.IBrinquedo;
import br.ufrn.imd.interfaces.IBrinquedo2;

public class Boneco implements IBrinquedo, IBrinquedo2{

	@Override
	public void mover() {
		System.out.println("Movendo brinquedo!");
		
	}

	@Override
	public void setNome(String nome) {
		System.out.println(nome);
	}

	@Override
	public void brincar() {
		// TODO Auto-generated method stub
		
	}
}
