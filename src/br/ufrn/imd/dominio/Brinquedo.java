package br.ufrn.imd.dominio;

public abstract class Brinquedo {

	private double velocidade;
	
	private double aceleracao;
	
	private String nome;
	
	public abstract void mover();
	
	public void velocidade(int vel) {
		
	}
	
	public void velocidade(double vel) {
		
	}
	
	public void velocidade(float vel, double ac) {
		
	}

	public double getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(double velocidade) {
		this.velocidade = velocidade;
	}

	public double getAceleracao() {
		return aceleracao;
	}

	public void setAceleracao(double aceleracao) {
		this.aceleracao = aceleracao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
