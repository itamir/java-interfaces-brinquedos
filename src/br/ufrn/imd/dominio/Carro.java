package br.ufrn.imd.dominio;


public class Carro extends Brinquedo{

	private int ano;
	
	private String cor;
	
	private String marca;
	
	private String modelo;

	
	public Carro() {

	}
	
	public Carro(int ano, String marca) {
		this.ano = ano;
		this.marca = marca;
	}
	
	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	@Override
	public String toString() {
		return ano + " - " + marca;
	}
	
	@Override
	public void mover() {
		System.out.println("Move na terra!");
	}

}
