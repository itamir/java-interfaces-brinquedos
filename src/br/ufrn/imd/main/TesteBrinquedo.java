package br.ufrn.imd.main;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.imd.dominio.Aviao;
import br.ufrn.imd.dominio.Barco;
import br.ufrn.imd.dominio.Brinquedo;
import br.ufrn.imd.dominio.Carro;

public class TesteBrinquedo {

	public static void main(String[] args) {
		
		Brinquedo b1 = new Carro();
		b1.setNome("Carro");
		
		Brinquedo b2 = new Barco();
		b2.setNome("Barco");
		
		Brinquedo b3 = new Aviao();
		b3.setNome("Avi�o");
		
		List<Brinquedo> brinquedos = new ArrayList<Brinquedo>();
		brinquedos.add(b1);
		brinquedos.add(b2);
		brinquedos.add(b3);
		
		for (Brinquedo brinquedo : brinquedos) {
			brinquedo.mover();
			if(brinquedo instanceof Carro) {
				System.out.println("Isso � um carro");
			} else if (brinquedo instanceof Aviao) {
				System.out.println("Isso � um avi�o");
			} else if (brinquedo instanceof Barco) {
				System.out.println("Isso � barco");
			}
		}
		
	}
	
}
