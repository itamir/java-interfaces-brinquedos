package br.ufrn.imd.main;

import br.ufrn.imd.dominio.Carro;

public class TesteCarro {

	public static void main(String[] args) {
		
		Carro c1 = new Carro();
		c1.setAno(2015);
		c1.setMarca("Ferrari");
		System.out.println(c1);
		
		Carro c2 = new Carro(2015, "Mercedes");
		System.out.println(c2);
		
	}
	
}
